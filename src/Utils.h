/*
 * Utils.h
 *
 *  Created on: Dec 22, 2015
 *      Author: michaeldangana
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string>
#include <iostream>
#include <sstream>
#include <vector>
#include <iterator>

class Utils {
public:
	Utils();
	virtual ~Utils();
	static const bool verbose = false;
	static void error(const char* msg)
	{
	    perror(msg);
	    exit(0);
	}
	static void print(const char* msg, ...)
	{
		std::cout << msg << std::endl;
	}

	static std::vector<int> toVec(int ints[], int size) {
		std::vector<int> v(ints, ints + size);
		return v;
	}

	static std::vector<std::string> toStrings(std::vector<int> ints) {
		std::vector<std::string> v;
		for (int i = 0; i < ints.size(); i++) {
			std::stringstream ss;
			ss << ints[i];
			v.push_back(ss.str());
		}
		return v;
	}

	static std::string join(std::vector<std::string> strings,
			std::string delim) {
		std::string string = "";
		for (int i = 0; i < strings.size(); i++) {
			string += strings[i] + delim;
		}
		return string;
	}
};

#endif /* UTILS_H_ */
