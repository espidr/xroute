//============================================================================
// Name        : Xroute.cpp
// Author      : 
// Version     :
// Copyright   : uOttawa
// Description : XRoute SDN Server
//============================================================================

#include <iostream>
#include "poxinterop/socketclient.h"
#include "flowgenerator/mapping/linkstate.h"
#include "flowgenerator/mapping/topology.h"
#include "flowgenerator/querying/TensorQuery.h"
#include "Utils.h"

using namespace std;

void execTest() {
	Utils::print("execTest");
	XrouteData::Topology* topo = new XrouteData::Topology();
	std::string data[] = {
			"1|3|4|1|00-00-00-00-00-01.3 -> 00-00-00-00-00-04.1|((1, 3), (4, 1))|add",
			"1|5|6|1|00-00-00-00-00-01.5 -> 00-00-00-00-00-06.1|((1, 5), (6, 1))|add",
			"1|5|6|1|00-00-00-00-00-01.5 -> 00-00-00-00-00-06.1|((1, 5), (6, 1))|remove"
	};
	for (int i = 0; i < 3; i++) {
		if (Utils::verbose)
			printf("execTest: datum is %s\n", data[i].c_str());
		XrouteData::LinkState* linkState = new XrouteData::LinkState(data[i]);
		if (Utils::verbose)
					printf("execTest: linkState.dpid is %s, isadd is %s\n",
							linkState->dpid1.c_str(),
				(linkState->isAdd? "true" : "false"));
		if (linkState->isAdd)
			topo->add(*linkState);
		else
			topo->remove(*linkState);
		topo->toImage();
	}
}

void execSockets() {
	string poxIp = "192.168.56.101";
	int poxPort = 8881;
	PoxInterop::SocketClient* sclient =
			new PoxInterop::SocketClient(poxIp, poxPort);
	XrouteData::Topology* topology = new XrouteData::Topology();
	//sclient->send("hello pox!");
	bool cont = true;
	while (cont) {
		string datum = sclient->readSock();
		if (!datum.empty()) {
		    cout << "Received " << datum << endl;
		    if (datum.compare("terminate") == 0)
		    	cont = true;
		    else {
		    	XrouteData::LinkState* linkState = new XrouteData::LinkState(datum);
				if (linkState->isAdd)
					topology->add(*linkState);
				else
					topology->remove(*linkState);
				//topology->toImage();
				XrouteQuery::TensorQuery query = new XrouteQuery::TensorQuery();
				vector<XrouteData::FlowRule> flowRules = query.query(topology);
				for (int i=0; i< flowRules.size(); i++)
					HttpClient::send("http://" << poxIp << ":" << poxPort,
						Utils::join(flowRules[i].entries(), "&"));
		    }
		}
	}
}

int main() {
	cout << "!!!Xroute is up!!!" << endl;
	execSockets();
	cout << "!!!Xroute is done!!!" << endl;
	return 0;
}
