/*
 * TrainingMonitor.h
 *
 *  Created on: Dec 10, 2015
 *      Author: michaeldangana
 */

#ifndef FLOWGENERATOR_QUERYING_TRAININGMONITOR_H_
#define FLOWGENERATOR_QUERYING_TRAININGMONITOR_H_

namespace XrouteQuery {

class TrainingMonitor {
public:
	TrainingMonitor();
	virtual ~TrainingMonitor();
};

} /* namespace XrouteQuery */

#endif /* FLOWGENERATOR_QUERYING_TRAININGMONITOR_H_ */
