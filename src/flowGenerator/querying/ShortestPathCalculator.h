/*
 * ShortestPathCalculator.h
 *
 *  Created on: Dec 10, 2015
 *      Author: michaeldangana
 */

#ifndef FLOWGENERATOR_QUERYING_SHORTESTPATHCALCULATOR_H_
#define FLOWGENERATOR_QUERYING_SHORTESTPATHCALCULATOR_H_

#include "../mapping/topology.h"
#include <functional>
#include <queue>
#include <vector>

using namespace std;

namespace XrouteQuery {

class ShortestPathCalculator {
public:
	ShortestPathCalculator();
	virtual ~ShortestPathCalculator();
	static vector<int> shortestPath(int** graph, int* costs, int src, int dest) {
		// Does a Dijkstra
		auto comp = [costs](int a, int b) { return costs[a] > costs[b]; };
		priority_queue<int, vector<int>, decltype( comp ) > pq (comp);
		vector<vector<int> > paths;
		int curr = src;
		while (curr != dest && curr != -1) {
			for (int i = 0; i<XrouteData::max_size; i++) // Add all children of curr
				if (graph[curr][i] >= 0) {
					pq.push(graph[curr][i]); // Update priority queue
					for (int i = 0; i<paths.size(); i++) // Update paths
						if (paths[i][paths[i].size()-1] == curr)
							paths[i].push_back(graph[curr][i]);
				}
			curr = pq.top(); pq.pop();
		}
		for (int i = 0; i<paths.size(); i++) { // Decrement paths
			paths[i].pop_back();
			if (paths[i][paths[i].size()-1] == dest)
				return paths[i]; // Return first path ending with dest
		}

	}
	static vector< vector<int> > shortestPaths(int** graph, int* costs) {
		vector< vector<int> > paths;
		for (int i=0; i<XrouteData::max_size; i++)
			for (int j=0; j<XrouteData::max_size; j++)
				paths.push_back(shortestPath(graph, costs, i, j));
		return paths;
	}
};

} /* namespace XrouteQuery */

#endif /* FLOWGENERATOR_QUERYING_SHORTESTPATHCALCULATOR_H_ */
