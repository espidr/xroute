/*
 * TensorQuery.h
 *
 *  Created on: Dec 10, 2015
 *      Author: michaeldangana
 */

#ifndef FLOWGENERATOR_QUERYING_TENSORQUERY_H_
#define FLOWGENERATOR_QUERYING_TENSORQUERY_H_

#include <vector>
#include "../mapping/flowrule.h"
#include "../mapping/topology.h"

namespace XrouteQuery {

class TensorQuery {
public:
	TensorQuery();
	virtual ~TensorQuery();
	std::vector<XrouteData::FlowRule> query(XrouteData::Topology);
};

} /* namespace XrouteQuery */

#endif /* FLOWGENERATOR_QUERYING_TENSORQUERY_H_ */
