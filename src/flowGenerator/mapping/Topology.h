/*
 * Topology.h
 *
 *  Created on: Dec 10, 2015
 *      Author: michaeldangana
 */

#ifndef FLOWGENERATOR_MAPPING_TOPOLOGY_H_
#define FLOWGENERATOR_MAPPING_TOPOLOGY_H_

#include <vector>
#include "LinkState.h"

namespace XrouteData {

const int max_size = 1000;

class Topology {
	std::vector<LinkState> links;
    int image[max_size][max_size];
    int nodeId(std::string dpid, std::string port);
public:
	Topology();
	virtual ~Topology();
	void add(LinkState);
	void remove(LinkState);
	void toImage();
};

} /* namespace XrouteData */

#endif /* FLOWGENERATOR_MAPPING_TOPOLOGY_H_ */
