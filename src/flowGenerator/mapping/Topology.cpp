/*
 * Topology.cpp
 *
 *  Created on: Dec 10, 2015
 *      Author: michaeldangana
 */

#include "Topology.h"
#include "../../Utils.h"
#include <vector>
#include <string>
#include <functional>
#include <iostream>

using namespace std;

namespace XrouteData {

Topology::Topology() {
	//image = { };
}

Topology::~Topology() {
	links.clear();
	//image = { };
}

void Topology::add(LinkState linkState) {
	links.push_back(linkState);
	if (Utils::verbose) printf("Topology::add: topo.size=%d\n", links.size());
}

void Topology::remove(LinkState linkState) {
	vector<int> matches;
	for (unsigned i=0; i<links.size(); ++i)
	    if (links[i].equals(linkState))
	    	matches.push_back(i);
	if (Utils::verbose)
		printf("Topology::remove: matches=%s\n",
			Utils::join(Utils::toStrings(matches), ",").c_str());
	for (unsigned i=0; i<matches.size(); ++i)
	    links.erase(links.begin() + matches[i] - i);
	if (Utils::verbose)
		printf("Topology::remove: topo.size=%d\n", links.size());
}

int Topology::nodeId(string dpid, string port) {
	return (atoi(dpid.c_str()) + (max_size / 2)) +
			(atoi(port.c_str()) % (max_size / 2));
}

void Topology::toImage() {
	for (unsigned i=0; i<links.size(); ++i) {
		int node = nodeId(links[i].dpid1, links[i].port1);
		int next = nodeId(links[i].dpid2, links[i].port2);
		bool assigned = false;
		for (unsigned c=0; c<max_size && !assigned; c++) {
			if (image[node][c] == next)
				assigned = true;
			if (!assigned && image[node][c] <= 0) {
				image[node][c] = next; assigned = true;
			}
		}
		if (Utils::verbose)
			printf("Topology::toImage: node=%d, next=%d, node.image=%s\n",
				node, next,
				Utils::join(Utils::toStrings(
				Utils::toVec(image[node], max_size)), ",").c_str());
	}
}

} /* namespace XrouteData */
