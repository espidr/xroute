/*
 * RouterState.h
 *
 *  Created on: Dec 10, 2015
 *      Author: michaeldangana
 */

#ifndef FLOWGENERATOR_MAPPING_ROUTERSTATE_H_
#define FLOWGENERATOR_MAPPING_ROUTERSTATE_H_

namespace XrouteData {

class RouterState {
public:
	RouterState();
	virtual ~RouterState();
};

} /* namespace XrouteData */

#endif /* FLOWGENERATOR_MAPPING_ROUTERSTATE_H_ */
