/*
 * LinkState.cpp
 *
 *  Created on: Dec 10, 2015
 *      Author: michaeldangana
 */

#include "LinkState.h"
#include "../../Utils.h"
#include <string.h>
#include <vector>

using namespace std;

namespace XrouteData {

LinkState::LinkState(string linkState) {
	char * cstr = new char [linkState.length()+1];
	strcpy (cstr, linkState.c_str());
	char* tokens[7] = { };
	char* token = strtok(cstr, "|");
	int i = 0;
    while ( token != NULL ) {
	  tokens[i++] = token;
	  token = strtok(NULL, "|");
    }
    dpid1 = tokens[0]; port1 = tokens[1];
    dpid2 = tokens[2]; port2 = tokens[3];
    uni = tokens[4]; end = tokens[5];
    isAdd = strcmp(tokens[6], "add") == 0;
}

LinkState::~LinkState() {}

}

string XrouteData::LinkState::toString() {
	return "dpid1=" + dpid1 + ", port1=" + port1 +
			", dpid2=" + dpid2 + ", port2=" + port2 +
			", uni=" + uni + ", end=" + end;
}

bool XrouteData::LinkState::equals(LinkState other) {
    return toString().compare(other.toString()) == 0;
}
