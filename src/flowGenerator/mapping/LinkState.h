/*
 * LinkState.h
 *
 *  Created on: Dec 10, 2015
 *      Author: michaeldangana
 */

#ifndef FLOWGENERATOR_MAPPING_LINKSTATE_H_
#define FLOWGENERATOR_MAPPING_LINKSTATE_H_

#include "RouterState.h"
#include <string>

namespace XrouteData {

class LinkState: public RouterState {

public:
	std::string dpid1, dpid2;
	std::string port1, port2;
	std::string uni, end;
    bool isAdd;
	LinkState(std::string);
	virtual ~LinkState();
	std::string toString();
	bool equals(LinkState);
};

} /* namespace XrouteData */

#endif /* FLOWGENERATOR_MAPPING_LINKSTATE_H_ */
