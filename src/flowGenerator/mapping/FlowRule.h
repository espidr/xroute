/*
 * FlowRule.h
 *
 *  Created on: Dec 23, 2015
 *      Author: michaeldangana
 */

#ifndef FLOWGENERATOR_MAPPING_FLOWRULE_H_
#define FLOWGENERATOR_MAPPING_FLOWRULE_H_

namespace XrouteData {

class FlowRule {
public:
	FlowRule();
	virtual ~FlowRule();
};

} /* namespace XrouteData */

#endif /* FLOWGENERATOR_MAPPING_FLOWRULE_H_ */
