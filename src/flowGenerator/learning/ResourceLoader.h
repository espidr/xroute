/*
 * ResourceLoader.h
 *
 *  Created on: Dec 10, 2015
 *      Author: michaeldangana
 */

#ifndef FLOWGENERATOR_LEARNING_RESOURCELOADER_H_
#define FLOWGENERATOR_LEARNING_RESOURCELOADER_H_

namespace XrouteTraining {

class ResourceLoader {
public:
	ResourceLoader();
	virtual ~ResourceLoader();
};

} /* namespace XrouteTraining */

#endif /* FLOWGENERATOR_LEARNING_RESOURCELOADER_H_ */
