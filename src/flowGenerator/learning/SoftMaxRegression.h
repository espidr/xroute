/*
 * SoftMaxRegression.h
 *
 *  Created on: Dec 10, 2015
 *      Author: michaeldangana
 */

#ifndef FLOWGENERATOR_LEARNING_SOFTMAXREGRESSION_H_
#define FLOWGENERATOR_LEARNING_SOFTMAXREGRESSION_H_

namespace XrouteTraining {

class SoftMaxRegression {
public:
	SoftMaxRegression();
	virtual ~SoftMaxRegression();
};

} /* namespace XrouteTraining */

#endif /* FLOWGENERATOR_LEARNING_SOFTMAXREGRESSION_H_ */
