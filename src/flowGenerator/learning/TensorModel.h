/*
 * TensorModel.h
 *
 *  Created on: Dec 10, 2015
 *      Author: michaeldangana
 */

#ifndef FLOWGENERATOR_LEARNING_TENSORMODEL_H_
#define FLOWGENERATOR_LEARNING_TENSORMODEL_H_

namespace XrouteTraining {

class TensorModel {
public:
	TensorModel();
	virtual ~TensorModel();
};

} /* namespace XrouteTraining */

#endif /* FLOWGENERATOR_LEARNING_TENSORMODEL_H_ */
