/*
 * Splicer.h
 *
 *  Created on: Dec 10, 2015
 *      Author: michaeldangana
 */

#ifndef FLOWGENERATOR_TRAINING_SPLICER_H_
#define FLOWGENERATOR_TRAINING_SPLICER_H_

namespace XrouteTraining {

class Splicer {
public:
	Splicer();
	virtual ~Splicer();
};

} /* namespace XrouteTraining */

#endif /* FLOWGENERATOR_TRAINING_SPLICER_H_ */
