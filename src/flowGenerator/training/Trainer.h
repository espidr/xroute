/*
 * Trainer.h
 *
 *  Created on: Dec 10, 2015
 *      Author: michaeldangana
 */

#ifndef FLOWGENERATOR_TRAINING_TRAINER_H_
#define FLOWGENERATOR_TRAINING_TRAINER_H_

namespace XrouteTraining {

class Trainer {
public:
	Trainer();
	virtual ~Trainer();
};

} /* namespace XrouteTraining */

#endif /* FLOWGENERATOR_TRAINING_TRAINER_H_ */
