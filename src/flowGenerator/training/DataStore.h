/*
 * DataStore.h
 *
 *  Created on: Dec 10, 2015
 *      Author: michaeldangana
 */

#ifndef FLOWGENERATOR_TRAINING_DATASTORE_H_
#define FLOWGENERATOR_TRAINING_DATASTORE_H_

namespace XrouteTraining {

class DataStore {
public:
	DataStore();
	virtual ~DataStore();
};

} /* namespace XrouteTraining */

#endif /* FLOWGENERATOR_TRAINING_DATASTORE_H_ */
