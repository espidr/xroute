/*
 * Processor.h
 *
 *  Created on: Dec 10, 2015
 *      Author: michaeldangana
 */

#ifndef ANALYTICS_PROCESSOR_H_
#define ANALYTICS_PROCESSOR_H_

namespace XrouteAnalytics {

class Processor {
public:
	Processor();
	virtual ~Processor();
};

} /* namespace XrouteAnalytics */

#endif /* ANALYTICS_PROCESSOR_H_ */
