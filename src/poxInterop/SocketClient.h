/*
 * SocketClient.h
 *
 *  Created on: Dec 21, 2015
 *      Author: michaeldangana
 */

#ifndef POXINTEROP_SOCKETCLIENT_H_
#define POXINTEROP_SOCKETCLIENT_H_


#include <string>

namespace PoxInterop {

class SocketClient {
	char* host;
	int sockid, port;
    char buffer[256];
public:
	SocketClient(char*, int);
	virtual ~SocketClient();
	void send(char*);
	std::string readSock();
};

} /* namespace PoxInterop */

#endif /* POXINTEROP_SOCKETCLIENT_H_ */
