/*
 * PoxMapper.h
 *
 *  Created on: Dec 10, 2015
 *      Author: michaeldangana
 */

#ifndef POXINTEROP_POXMAPPER_H_
#define POXINTEROP_POXMAPPER_H_

namespace XrouteAnalytics {

class PoxMapper {
public:
	PoxMapper();
	virtual ~PoxMapper();
};

} /* namespace XrouteAnalytics */

#endif /* POXINTEROP_POXMAPPER_H_ */
