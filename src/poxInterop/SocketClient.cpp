/*
 * SocketClient.cpp
 *
 *  Created on: Dec 21, 2015
 *      Author: michaeldangana
 */

#include "SocketClient.h"
#include "../utils.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <netdb.h>
#include <fcntl.h>

namespace PoxInterop {

SocketClient::SocketClient(char* _host, int _port) {
    struct sockaddr_in serv_addr;
    struct hostent *server;
	host = _host;
    port = _port;
    sockid = socket(AF_INET, SOCK_STREAM, 0);
    if (sockid < 0)
        Utils::error("ERROR opening socket");
    //if (fcntl(sockid, F))
    // If iMode = 0, blocking is enabled;
    // If iMode != 0, non-blocking mode is enabled.
    //int iMode = 0;
    //ioctl(sockid, FIONBIO, &iMode);

    server = gethostbyname(_host);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr,
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(_port);
    if (connect(sockid,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
        Utils::error("ERROR connecting");
    int flags = fcntl(sockid, F_GETFL);
    if (flags & O_ASYNC) {
    	Utils::print("yay, its blocking already");
    }
    flags |= O_ASYNC;
    if (fcntl(sockid, F_SETFL, flags) & O_ASYNC)
    {
    //non blocking
    	Utils::print( "fcntl shows it is now blocking\n" );
    }
}

SocketClient::~SocketClient() {
	close(sockid);
}

void SocketClient::send(char* buffer)
{
    long n = write(sockid, buffer, strlen(buffer));
    if (n < 0)
         Utils::error("ERROR writing to socket");
}

std::string SocketClient::readSock()
{
	bzero(buffer,256);
	long n = recv(sockid,buffer,255,MSG_WAITALL);
	if (n < 0)
		 Utils::error("ERROR reading from socket");
	return std::string(buffer);
}


} /* namespace PoxInterop */
