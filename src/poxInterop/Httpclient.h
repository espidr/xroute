/*
 * Httpclient.h
 *
 *  Created on: Dec 23, 2015
 *      Author: michaeldangana
 */

#ifndef POXINTEROP_HTTPCLIENT_H_
#define POXINTEROP_HTTPCLIENT_H_

#include <stdio.h>
#include <curl/curl.h>

namespace PoxInterop {

class Httpclient {
public:
	Httpclient();
	virtual ~Httpclient();

	static int send(String host, String data)
	{
	  CURL *curl;
	  CURLcode res;

	  curl = curl_easy_init();
	  if(curl) {
	    curl_easy_setopt(curl, CURLOPT_URL, host << data);
	    /* url is redirected, so we tell libcurl to follow redirection */
	    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

	    /* Perform the request, res will get the return code */
	    res = curl_easy_perform(curl);
	    /* Check for errors */
	    if(res != CURLE_OK)
	      fprintf(stderr, "curl_easy_perform() failed: %s\n",
	              curl_easy_strerror(res));

	    /* always cleanup */
	    curl_easy_cleanup(curl);
	  }
	  return 0;
	}
};

} /* namespace PoxInterop */

#endif /* POXINTEROP_HTTPCLIENT_H_ */
