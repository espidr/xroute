# README #

Xroute is a application used for orchestrating flows on SDN networks. It monitors SDN controllers for network events for link state and topology discovery, and generates flow rules.

This is currently in development and any issues found can be logged on JIRA at https://xroute.atlassian.net. Thanks for visiting this page. Happy coding!

### What is this repository for? ###

* It is a SDN network flow orchestrator using machine learning for fast shortest path calculations
* Version 1
* [Learn Xroute](https://bitbucket.org/tutorials/xroutedemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact